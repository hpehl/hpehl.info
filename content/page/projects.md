+++
date = "2015-03-31T01:58:30+02:00"
draft = true
title = "Projects"

+++
<iframe src="http://githubbadge.appspot.com/badge/hpehl?s=1&a=0" style="float: right; margin-left: 5em; margin-bottom: 1em; border: 0; height: 142px; width: 200px; overflow: hidden;" frameBorder=0></iframe> You
can find most of the projects I'm working on at [GitHub](https://github.com/hpehl). Some of my recent work include
working on the following projects:

* [WildFly](http://www.wildfly.org/) (fka JBoss AS): Working on management related subjects like the Admin Console.

* Polyglot DMR: [DMR.js](https://github.com/hal/dmr.js), [DMR.dart](https://github.com/hal/dmr.dart), [DMR.scala](https://github.com/hal/dmr.scala)

* [Schlawiner](https://schlawiner-hpehl.rhcloud.com/): Puzzle and dice game based on Errai. Schlawiner is hosted at
[OpenShift](https://www.openshift.com/) and also available in the
[Chrome Web Store](https://chrome.google.com/webstore/detail/schlawiner/kdejnglllpddknpckoffanchkbjhjlkb) and the
[Firefox Marketplace](https://marketplace.firefox.com/app/schlawiner/).

* [Karaka](http://karaka-d8.appspot.com/): A personal time recording app deployed on Google AppEngine.

* [Piriti](http://hpehl.info/piriti/): JSON and XML mapper for GWT based on reasonable defaults, a handful of
annotations and deferred binding.

Besides I like to contribute to open source projects like [GWTP](https://github.com/ArcBees/GWTP). For a more detailed
overview take a look at my [open source report card](http://osrc.dfm.io/hpehl).
