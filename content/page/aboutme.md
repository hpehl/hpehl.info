+++
date = "2015-03-31T01:58:56+02:00"
draft = true
title = "Hi there!"

+++
I'm a Senior Software Engineer at JBoss by Red Hat. I'm part of the WildFly team and currently working on management related subjects like the Admin Console.

If you want to get in contact, use one of the following channels:

<ul>
<li><a href="https://plus.google.com/112941298216109713269/about"><i class="fa fa-google-plus-square fa-2x"></i></a><span>&mdash;&nbsp;Google+</span></li>
<li><a href="https://twitter.com/haraldpehl"><i class="fa fa-twitter fa-2x"></i></a><span>&mdash;&nbsp;Twitter</span></li>
<li><a href="http://de.linkedin.com/pub/harald-pehl/23/981/802"><i class="fa fa-linkedin fa-2x icon-linkedin"></i></a><span>&mdash;&nbsp;LinkedIn</span></li>
<li><a href="mailto:harald.pehl@gmail.com"><i class="fa fa-envelope fa-2x"></i></a><span>&mdash;&nbsp;Email</span></li>
</ul>

This is a personal blog. The opinions expressed here represent my own and not those of my employer.
