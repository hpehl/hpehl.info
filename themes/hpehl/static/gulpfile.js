/*!
 * gulp
 * $ npm install gulp-sass gulp-autoprefixer gulp-minify-css gulp-rename gulp-copy del --save-dev
 */

// Load plugins
var gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    minifycss = require('gulp-minify-css'),
    rename = require('gulp-rename'),
    copy = require('gulp-copy'),
    del = require('del');

// SCSS
gulp.task('scss', function() {
    return gulp.src('scss/main.scss')
        .pipe(sass())
        .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
        .pipe(gulp.dest('css'))
        .pipe(rename({ suffix: '.min' }))
        .pipe(minifycss())
        .pipe(gulp.dest('css'));
});

// 3rd party deps
gulp.task('deps', function(cb) {
    gulp.src('bower_components/font-awesome/css/*.css')
        .pipe(copy('css', {prefix: 3}));
    gulp.src('bower_components/font-awesome/fonts/*')
        .pipe(copy('fonts', {prefix: 3}));
});

// Clean
gulp.task('clean', function(cb) {
    del(['css/main*.css'], cb)
});

// Default task
gulp.task('default', ['clean', 'deps', 'scss']);

// Watch
gulp.task('watch', function() {
    // Watch .scss files
    gulp.watch('scss/**/*.scss', ['scss']);
});
